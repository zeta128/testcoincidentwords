﻿using System;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestCoincidentWords.Controllers;

namespace TestCoincidentWords.Tests
{
    [TestClass]
    public class LoadFileTest
    {
        [TestMethod]
        public void TestLoadFiles(HttpPostedFileBase fileTest)
        {
            const bool expected = true;
            bool actual = HomeController.isValidFileType(fileTest.ContentType);
            Assert.AreEqual(expected,actual);
        }
    }
}
