﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;


namespace WSProcessFiles
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "WSProcessFiles" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione WSProcessFiles.svc o WSProcessFiles.svc.cs en el Explorador de soluciones e inicie la depuración.
    [Serializable]
    public class WSProcessFiles : IWSProcessFiles
    {
        //Escribe en el archivo de RESULTADOS.txt si un registro dado existe o no dentro del archivo CONTENIDO y modifica el array de contenido y lo retorna en caso de que exista 
        public String[] ModifyContentFile(string[] arrayContent, string[] lineRegisteredArray, string lineRegistered)
        {
            int cont = 0;
            foreach (String reg in lineRegisteredArray)
            {
                if (arrayContent.Contains(reg))
                {
                    cont++;
                }
                var indexToRemove = Array.IndexOf(arrayContent, reg);
                arrayContent = arrayContent.Where((val, idx) => idx != indexToRemove).ToArray();
            }
            if (cont == lineRegisteredArray.Length)
            {
             
                return arrayContent;
            }
            else
            {
               
                return null;
            }
        }
    }
}
