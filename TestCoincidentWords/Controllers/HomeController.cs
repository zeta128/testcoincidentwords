﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Windows.Forms;

namespace TestCoincidentWords.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        //Carga los archivos seleccionados dentro del proyecto en la ruta Content/files
      
        [HttpPost]
        public ActionResult LoadFiles(HttpPostedFileBase fileContent, HttpPostedFileBase fileRegistered)
        {
            var contentFileName = ""; 
            var registeredFileName="";
            if ( fileContent!=null&&fileRegistered!=null)
            {
                if (isValidFileType(fileContent.ContentType) && isValidFileType(fileRegistered.ContentType))
                {
                    //Ruta y  guardado en una ruta dentro del sistema del archivo CONTENIDO
                    contentFileName = Path.GetFileName(fileContent.FileName);
                    var pathFileContent = Path.Combine(Server.MapPath("~/Content/files"), contentFileName);
                    fileContent.SaveAs(pathFileContent);

                    //Ruta y  guardado en una ruta dentro del sistema del archivo REGISTRADOS
                    registeredFileName = Path.GetFileName(fileRegistered.FileName);
                    var pathFileRegistered = Path.Combine(Server.MapPath("~/Content/files"), registeredFileName);
                    fileRegistered.SaveAs(pathFileRegistered);
                    
                    
                    ReadFiles(contentFileName, registeredFileName);                   
                }
                else
                {
                    String error="";
                    if (isValidFileType(fileContent.ContentType))
                    {
                        error += "Formato invalido, debe seleccionar un archivo de texto plano para cargar en CONTENIDO\n";
                    }
                    if (isValidFileType(fileRegistered.ContentType))
                    {
                        error = "Formato invalido, debe seleccionar un archivo de texto plano para cargar en REGISTRADOS\n";
                    }                
                    MessageBox.Show(error);
                }
            }
            
            else
            {
                String error = "";
                if (fileContent==null)
                {
                    error += "Por favor cargue el archivo CONTENIDO.txt\n";
                }
                if (fileRegistered==null)
                {
                    error += "Por favor cargue el archivo REGISTRADOS.txt";
                }
                MessageBox.Show(error);
            }


            return RedirectToAction("Index");

        }
        
        //Verifica si el tipo de archivo es de texto plano
        public static bool isValidFileType(string fileType)
        {
            return fileType.Equals("text/plain");
        }

        //Convierte el archivo de texto cuyo nombre ingresa por parámetro en un array de String y lo retorna
        public String[] ConvertFileInArray(String FileName)
        {
            StreamReader FileRead = new StreamReader(AppDomain.CurrentDomain.BaseDirectory+"Content\\files\\" + FileName);
            try
            {
                return FileRead.ReadToEnd().Replace("\r", "").Replace("\t", "").Split('\n');
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
            FileRead.Close();
            return null;
        }

      
        public void ReadFiles(String contentFileName, String registeredFileName)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + "Content\\files\\RESULTADOS.txt";
            try
            {
                String[] arrayContent = ConvertFileInArray(contentFileName);
                String[] arrayRegistered = ConvertFileInArray(registeredFileName);
                StreamWriter FileResult = new StreamWriter(path);
                
                //Declaración del Web Service
                using (WSProcessFiles.WSProcessFilesClient WSProcessFiles= new WSProcessFiles.WSProcessFilesClient())
                {
                    foreach (String elementRegistered in arrayRegistered)
                    {
                        //Convierte el elemento del array de registrados en un array de caracteres
                        String[] lineRegisteredArray = elementRegistered.ToCharArray().Select(c => c.ToString()).ToArray(); ;
                        
                        var isModifyContentFile = WSProcessFiles.ModifyContentFile(arrayContent, lineRegisteredArray, elementRegistered);

                        if (isModifyContentFile!=null)
                        {

                            FileResult.WriteLine(elementRegistered + "-->Sí Existe");
                            arrayContent = isModifyContentFile;
                        }
                        else
                        {
                            FileResult.WriteLine(elementRegistered + "-->No Existe");
                        }
                        
                    }
                    FileResult.Close();
                    
                    Process.Start(@"notepad.exe", path);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }

        }
       
        }
}